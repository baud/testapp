require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get landing" do
    get :landing
    assert_response :success
  end

  test "should get main" do
    get :main
    assert_response :success
  end

end
